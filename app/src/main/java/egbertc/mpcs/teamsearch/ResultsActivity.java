package egbertc.mpcs.teamsearch;

import android.app.ActionBar;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

// http://www.androidhive.info/2013/11/android-working-with-action-bar/

public class ResultsActivity extends Activity {

    ArrayAdapter<String> adapter;
    ListView lView;
    TextView rText;
    ArrayList<String> words;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        Log.d("ResultsActivity", "in results");

        words = new ArrayList<>();
        Collections.addAll(words, getResources().getStringArray(R.array.words_array));

        lView = (ListView) findViewById(R.id.listView);
        rText = (TextView) findViewById(R.id.resultText);
        if(words != null && words.size() > 0)
        {
            adapter = new ArrayAdapter<>(this, R.layout.list_item, R.id.text, words);
            lView.setAdapter(adapter);

        }
        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    /**
     * Handling intent data
     */
    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);


            Toast.makeText(this,query,Toast.LENGTH_LONG).show();

            if(words.contains(query))
            {
                rText.setText("Found: " + query);
            }
            else
                rText.setText(query + " Not Found");
        }

    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_results, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
